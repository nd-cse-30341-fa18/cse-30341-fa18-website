title:      "Reading 09: Swapping"
icon:       fa-book
navigation: []
internal:
external:
body:       |

    **Everyone**:

    Next week, after finishing up [paging], we will explore how an OS uses
    [swapping] to allow for virtual address spaces larger than physical memory
    along with various page replacement algorithms.

    <div class="alert alert-info" markdown="1">

    #### <i class="fa fa-search"></i> TL;DR

    For this reading assignment, you are to read about [swapping], page
    replacement policies, and submit your responses to the [Reading 09 Quiz].

    </div>

    [paging]:   https://en.wikipedia.org/wiki/Paging
    [swapping]: https://en.wikipedia.org/wiki/Paging#History

    <img src="static/img/ostep.jpg" class="pull-right">

    ## Reading

    The readings for **Tuesday, November 6** are:

    1. [Operating Systems: Three Easy Pieces]

        - <p>Swapping: Mechanisms</p>
        - <p>Swapping: Policies</p>
        - <p>Case Study: VAX/VMS</p>
        - <p>Summary</p>

    ## Quiz

    Once you have done the readings, answer the following [Reading 09 Quiz]
    questions:

    <div id="quiz-questions"></div>

    <div id="quiz-responses"></div>

    <script src="static/js/dredd-quiz.js"></script>
    <script>
    loadQuiz('static/json/reading09.json');
    </script>

    ## Submission

    To submit you work, follow the same process outlined in [Reading 00]:

        :::bash
        $ git checkout master                 # Make sure we are in master branch
        $ git pull --rebase                   # Make sure we are up-to-date with GitLab

        $ git checkout -b reading09           # Create reading09 branch and check it out

        $ cd reading09                        # Go into reading09 folder
        $ $EDITOR answers.json                # Edit your answers.json file

        $ ../.scripts/submit.py               # Check reading09 quiz
        Submitting reading09 assignment ...
        Submitting reading09 quiz ...
             Q01 0.60
             Q02 0.40
             Q03 0.60
             Q04 0.30
             Q05 0.40
             Q06 0.30
             Q07 0.10
             Q08 0.20
             Q09 0.10
           Score 3.00

        $ git add answers.json                # Add answers.json to staging area
        $ git commit -m "Reading 09: Done"    # Commit work

        $ git push -u origin reading09        # Push branch to GitLab

    Remember to [create a merge request] and assign the appropriate TA from the
    [Reading 09 TA List].

    [busybox]:                                  https://www.busybox.net/
    [Operating Systems: Three Easy Pieces]:     http://pages.cs.wisc.edu/~remzi/OSTEP/
    [GitLab]:                                   https://gitlab.com
    [Reading 00]:                               reading00.html
    [Reading 09 Quiz]:                          static/json/reading09.json
    [JSON]:                                     http://www.json.org/
    [git-branch]:                               https://git-scm.com/docs/git-branch
    [dredd]:                                    https://dredd.h4x0r.space
    [create a merge request]:                   https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html
    [Reading 09 TA List]:                       reading09_tas.html
